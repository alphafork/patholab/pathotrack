import $ from 'jquery';
window.jQuery = $;
window.$ = $;
import 'bootstrap';
import 'tabler/dist/js/tabler.min.js';
import 'tabler/dist/css/tabler.min.css';
import 'datatables.net'
import 'datatables.net-bs4'
import 'flatpickr/dist/flatpickr.min.css'
import 'flatpickr/dist/themes/airbnb.css'
import 'flatpickr/dist/flatpickr.min.js'
