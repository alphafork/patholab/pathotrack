"""pathotrack URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include,path
import tinymce.urls
from django.conf.urls.static import static # new
from . import settings

 

urlpatterns = [
    path('admin/', admin.site.urls),
    path('patients/', include('applications.patient_registration.urls', namespace='patients')), #every app should be added as `applications.<app name>.urls`
    path('users/', include('applications.users.urls', namespace='users')), 
    path('', include('applications.users.urls', namespace='users')), 
    path('reports/general/', include('applications.general_report.urls', namespace='general-reports')),
    path('reports/cap/', include('applications.cap_report.urls', namespace='cap-reports')),
    path('reports/consolidated/', include('applications.consolidated_report.urls', namespace='consolidated-reports')),
    path('tinymce/', include('tinymce.urls')),
]


if settings.DEBUG: # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
