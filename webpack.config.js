const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: {
    bundle: './staticfiles/src/index.js',
  },

  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './staticfiles/dist'),
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },

      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },

      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader',
        ],
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [{
          from: path.resolve(__dirname, './staticfiles/src/django_tinymce'),
          to: 'django_tinymce',
        },
        {
          from: path.resolve(__dirname, './staticfiles/src/tinymce'),
          to: 'tinymce',
        },
      ],
      options: {
        concurrency: 100,
      },
    }),
  ],

  devServer: {
    hot: true,
    publicPath: '/static/',
    port: 3000,
    proxy: [
      {
        context: [
          '**',
          '!/static/**',
          '!/media/**'
        ],
        target: 'http://localhost:8000',
        changeOrigin: true,
      },
    ],
  },
};
