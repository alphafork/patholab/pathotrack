class SessionManagementMixin():

    """This mixin will remove every session variables except the
    ones listed under session_variables"""

    session_vars = []
    __auth_session_variables=['_auth_user_id','_auth_user_backend','_auth_user_hash']

    def setup(self, request, *args, **kwargs):

        __session_vars_all = self.session_vars + self.__auth_session_variables
        for key in list(request.session.keys()):
            if key not in __session_vars_all:
                del request.session[key]
            else:
                pass
        request.session.modified = True
        return super().setup(request, *args, **kwargs)
