from django.db import models
from applications.general_report.models.start_diagnosis_model import StartDiagnosis
from applications.general_report.models.general_report_model import GeneralReport
from applications.patient_registration.models.patient_registration_model import PatientRegistration
from applications.cap_report.models.breast_dcis_biopsy_model import Procedure, Specimen_laterality, Tumor_site, Histologic_type, Nuclear_grade, Necrosis, Microcalcifications

class Tumor_size(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value

class Glandular_tubular_differentiation(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value

class Nuclear_pleomorphism(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value

class Mitotic_rate(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value

class Overall_grade(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value

class Ductal_carcinoma_insitu(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value

class Architectural_patterns(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value

class Lymphovascular_invasion(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value



class BreastInvasiveBiopsy(models.Model):
    class Meta:
        db_table = 'breast_invasive_biopsy'

    acc_no = models.OneToOneField(StartDiagnosis, on_delete=models.CASCADE, primary_key=True, editable=False)
    patient_id = models.ForeignKey(PatientRegistration, on_delete=models.CASCADE, editable=False)

    procedure = models.ForeignKey(Procedure, on_delete=models.CASCADE, null=True, blank=True)
    procedure_note = models.CharField(max_length=255, blank=True)

    specimen_laterality = models.ForeignKey(Specimen_laterality, on_delete=models.CASCADE, null=True, blank=True)
    specimen_laterality_note = models.CharField(max_length=255, blank=True)

    tumor_site = models.ForeignKey(Tumor_site, on_delete=models.CASCADE, null=True, blank=True)
    tumor_site_note = models.CharField(max_length=255, blank=True)

    histologic_type = models.ForeignKey(Histologic_type, on_delete=models.CASCADE, null=True, blank=True)
    histologic_type_note = models.CharField(max_length=255, blank=True)

    nuclear_grade = models.ForeignKey(Nuclear_grade, on_delete=models.CASCADE, null=True, blank=True)
    nuclear_grade_note = models.CharField(max_length=255, blank=True)

    necrosis = models.ForeignKey(Necrosis, on_delete=models.CASCADE, null=True, blank=True)
    necrosis_note = models.CharField(max_length=255, blank=True)

    microcalcifications = models.ForeignKey(Microcalcifications, on_delete=models.CASCADE, null=True, blank=True)
    microcalcifications_note = models.CharField(max_length=255, blank=True)

    tumor_size = models.ForeignKey(Tumor_size, on_delete=models.CASCADE, null=True, blank=True)
    tumor_size_note = models.CharField(max_length=255, blank=True)

    glandular_tubular_differentiation = models.ForeignKey(Glandular_tubular_differentiation, on_delete=models.CASCADE, null=True, blank=True)
    glandular_tubular_differentiation_note = models.CharField(max_length=255, blank=True)

    nuclear_pleomorphism = models.ForeignKey(Nuclear_pleomorphism, on_delete=models.CASCADE, null=True, blank=True)
    nuclear_pleomorphism_note = models.CharField(max_length=255, blank=True)

    mitotic_rate = models.ForeignKey(Mitotic_rate, on_delete=models.CASCADE, null=True, blank=True)
    mitotic_rate_note = models.CharField(max_length=255, blank=True)

    overall_grade = models.ForeignKey(Overall_grade, on_delete=models.CASCADE, null=True, blank=True)
    overall_grade_note = models.CharField(max_length=255, blank=True)

    ductal_carcinoma_insitu = models.ForeignKey(Ductal_carcinoma_insitu, on_delete=models.CASCADE, null=True, blank=True)
    ductal_carcinoma_insitu_note = models.CharField(max_length=255, blank=True)

    architectural_patterns = models.ForeignKey(Architectural_patterns, on_delete=models.CASCADE, null=True, blank=True)
    architectural_patterns_note = models.CharField(max_length=255, blank=True)

    lymphovascular_invasion = models.ForeignKey(Lymphovascular_invasion, on_delete=models.CASCADE, null=True, blank=True)
    lymphovascular_invasion_note = models.CharField(max_length=255, blank=True)



    # date_created =
    # date_updated =
    # form_status =
    # lab_techinician =
    # senior_technician =
    # user_updated =

    def get_fields(self):
        return [(field.verbose_name, field.value_from_object(self)) for field in self.__class__._meta.fields]
