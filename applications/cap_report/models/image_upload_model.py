from django.db import models
from applications.general_report.models.start_diagnosis_model import StartDiagnosis
from applications.patient_registration.models.patient_registration_model import PatientRegistration


class ImageUpload(models.Model):
    class Meta:
        db_table = 'report_images'


    acc_no = models.ForeignKey(StartDiagnosis, on_delete=models.CASCADE, editable=False)
    patient_id = models.ForeignKey(PatientRegistration, on_delete=models.CASCADE, editable=False)
    specimen_image = models.ImageField(upload_to='images/%Y/%m/%d') # ImageField needs pillow to be installed. or use FileField

    # File name = (only if an additional naming is needed)

    specimen_name = models.CharField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    # user_created =
    # user_updated =


    # def save(self):

    #     if not self.id and not self.specimen_image:
    #         return

    #     super(ImageUpload, self).save()

    #     image = ImageUpload.open(self.specimen_image)
    #     (width, height) = image.size

    #     "Max width and height 800"
    #     if (800 / width < 800 / height):
    #         factor = 800 / height
    #     else:
    #         factor = 800 / width

    #     size = ( width / factor, height / factor)
    #     image = image.resize(size, ImageUpload.ANTIALIAS)
    #     image.save(self.photo.path)
