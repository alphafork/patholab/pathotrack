from django.db import models
from applications.general_report.models.start_diagnosis_model import StartDiagnosis
from applications.general_report.models.general_report_model import GeneralReport
from applications.patient_registration.models.patient_registration_model import PatientRegistration

class Procedure(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class Specimen_laterality(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class Tumor_site(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class Histologic_type(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class Nuclear_grade(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class Necrosis(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class Microcalcifications(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class BreastDCISBiopsy(models.Model):
    class Meta:
        db_table = 'breast_dcis_biopsy'

    acc_no = models.OneToOneField(StartDiagnosis, on_delete=models.CASCADE, primary_key=True, editable=False)
    patient_id = models.ForeignKey(PatientRegistration, on_delete=models.CASCADE, editable=False)

    procedure = models.ForeignKey(Procedure, on_delete=models.CASCADE, null=True, blank=True)
    procedure_note = models.CharField(max_length=255, blank=True)

    specimen_laterality = models.ForeignKey(Specimen_laterality, on_delete=models.CASCADE, null=True, blank=True)
    specimen_laterality_note = models.CharField(max_length=255, blank=True)

    tumor_site = models.ForeignKey(Tumor_site, on_delete=models.CASCADE, null=True, blank=True)
    tumor_site_note = models.CharField(max_length=255, blank=True)

    histologic_type = models.ForeignKey(Histologic_type, on_delete=models.CASCADE, null=True, blank=True)
    histologic_type_note = models.CharField(max_length=255, blank=True)

    nuclear_grade = models.ForeignKey(Nuclear_grade, on_delete=models.CASCADE, null=True, blank=True)
    nuclear_grade_note = models.CharField(max_length=255, blank=True)

    necrosis = models.ForeignKey(Necrosis, on_delete=models.CASCADE, null=True, blank=True)
    necrosis_note = models.CharField(max_length=255, blank=True)

    microcalcifications = models.ForeignKey(Microcalcifications, on_delete=models.CASCADE, null=True, blank=True)
    microcalcifications_note = models.CharField(max_length=255, blank=True)

    # date_created =
    # date_updated =
    # form_status =
    # lab_techinician =
    # senior_technician =
    # user_updated =

    def get_fields(self):
        return [(field.verbose_name, getattr(self, field.name)) for field in self._meta.get_fields()]
