from django.db import models
from applications.general_report.models.start_diagnosis_model import StartDiagnosis
from applications.patient_registration.models.patient_registration_model import PatientRegistration
from tinymce.models import HTMLField


class NonSynoptic(models.Model):
    class Meta:
        db_table = 'non_synoptic_reports'

    acc_no = models.OneToOneField(StartDiagnosis, on_delete=models.CASCADE, primary_key=True, editable=False)
    patient_id = models.ForeignKey(PatientRegistration, on_delete=models.CASCADE, editable=False)
    non_synoptic_diagnosis = HTMLField('Diagnosis', null=True, blank=True)

    # date_created =
    # date_updated =
    # user_created =
    # user_updated =

    def get_fields(self):
        return [(field.verbose_name, field.value_from_object(self)) for field in self.__class__._meta.fields]
