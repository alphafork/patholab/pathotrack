from django.apps import AppConfig


class CapReportConfig(AppConfig):
    name = 'applications.cap_report'
    verbose_name = 'Cap report (Synoptic)'
