from django.views.generic import CreateView, UpdateView
from applications.general_report.models.general_report_model import GeneralReport
from applications.general_report.models.start_diagnosis_model import StartDiagnosis
from applications.patient_registration.models.patient_registration_model import PatientRegistration
from applications.cap_report.models.image_upload_model import ImageUpload
from django.shortcuts import redirect, get_object_or_404
from django.http import HttpResponseForbidden  # for testing
from utils.mixins import SessionManagementMixin
from django.conf import settings
import os
import importlib


# Class to create patient entry using CreateView
class CAPReportCreate(CreateView):
    fields = "__all__"
    template_name = "cap_report_form.html"
    session_vars =  ['patient_id','acc_no','patient_name','patient_age','patient_sex','model_name','model_class']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pathology_id'] = StartDiagnosis.objects.get(pk=self.request.session['acc_no']).pathology_id
        # to list images if exists
        if ImageUpload.objects.filter(acc_no = self.request.session['acc_no']):
            context['images'] = ImageUpload.objects.filter(acc_no = self.request.session['acc_no'])
        return context
        
    def get_queryset(self): # to import model dynamically
        model_name = importlib.import_module(self.request.session['model_name'])
        model_class = getattr(model_name, self.request.session['model_class'])
        return model_class.objects.all()

    def form_valid(self, form):
        submit_stat = self.request.POST.get("submit")
        acc_no = self.request.session['acc_no']
        patient_id = self.request.session['patient_id']

        if(submit_stat == "cancel"): # when "Cancel" button pressed
            StartDiagnosis.objects.filter(pk=acc_no).update(report_status="draft")
            return redirect('patients:details')

        # to delete user selected images (check the same section in update view for further info)
        if (self.request.POST):
            for deleted_image in self.request.POST.getlist('image_deleted'):
                if ImageUpload.objects.get(pk = deleted_image).acc_no_id == self.request.session['acc_no']:
                    ImageUpload.objects.get(pk = deleted_image).specimen_image.delete()
                    ImageUpload.objects.get(pk = deleted_image).delete()

        form.instance.acc_no = StartDiagnosis.objects.get(pk=acc_no)
        form.instance.patient_id = PatientRegistration.objects.get(pk=patient_id)
        report = form.save()

        # Image upload
        if (self.request.FILES):
            for image in self.request.FILES.getlist('image_field'): #image_field be the name of the input field
                instance = ImageUpload(acc_no = StartDiagnosis.objects.get(pk=acc_no), specimen_image = image, specimen_name= image.name.split("/")[-1].split(".")[0], patient_id = PatientRegistration.objects.get(pk=patient_id))
                instance.save()

        if(submit_stat == "save"): # when pressed "Submit & Return"
            StartDiagnosis.objects.filter(pk=acc_no).update(report_status="draft")
            return redirect('patients:details')

        if(submit_stat == "continue"): # when pressed "Submit & Continue"
            self.request.session['acc_no'] = acc_no
            StartDiagnosis.objects.filter(pk=acc_no).update(report_status="submitted")
            return redirect('consolidated-reports:details')

        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]


class CAPReportUpdate(UpdateView):
    template_name = "cap_report_form.html"
    fields = "__all__"
    session_vars =  ['patient_id','acc_no','patient_name','patient_age','patient_sex','model_name','model_class']
    # StartDiagnosis.objects.filter(pk=acc_no).update(report_status="wip")


    def get_object(self): # to get object with pk with person id
        model_name = importlib.import_module(self.request.session['model_name'])
        model_class = getattr(model_name, self.request.session['model_class'])
        return get_object_or_404(model_class, pk=self.request.session['acc_no'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pathology_id'] = StartDiagnosis.objects.get(pk=self.request.session['acc_no']).pathology_id
        if ImageUpload.objects.filter(acc_no = self.request.session['acc_no']):
            context['images'] = ImageUpload.objects.filter(acc_no = self.request.session['acc_no'])
        return context

    def form_valid(self, form):
        submit_stat = self.request.POST.get("submit")
        patient_id = self.request.session['patient_id']
        acc_no = self.request.session['acc_no']

        if(submit_stat == "cancel"): # when "Cancel" button pressed
            StartDiagnosis.objects.filter(pk=acc_no).update(report_status="draft")
            return redirect('patients:details')

        # to delete user selected images
        if (self.request.POST):
            for deleted_image in self.request.POST.getlist('image_deleted'):
                if ImageUpload.objects.get(pk = deleted_image).acc_no_id == self.request.session['acc_no']: # checking if the image is from same report
                    ImageUpload.objects.get(pk = deleted_image).specimen_image.delete()        
                    ImageUpload.objects.get(pk = deleted_image).delete()
                    # [FIXME] add the unauthorised request response page redirection

        form.instance.acc_no = StartDiagnosis.objects.get(pk = acc_no)
        form.instance.patient_id = PatientRegistration.objects.get(pk = patient_id)
        form.save()

        # Image upload
        if (self.request.FILES):
            for image in self.request.FILES.getlist('image_field'): #image_field be the name of the input field
                instance = ImageUpload(acc_no = StartDiagnosis.objects.get(pk=acc_no), specimen_image = image, specimen_name= image.name.split("/")[-1].split(".")[0], patient_id = PatientRegistration.objects.get(pk=patient_id))
                instance.save()

        if(submit_stat == "continue"): # when pressed "Submit & Continue"
            self.request.session['acc_no'] = acc_no
            StartDiagnosis.objects.filter(pk = acc_no).update(report_status="submitted")
            return redirect('consolidated-reports:details')
        
        elif(submit_stat == "save"): # when pressed "Submit & Return"
            StartDiagnosis.objects.filter(pk = acc_no).update(report_status="draft")
            return redirect('patients:details')
            
        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]
