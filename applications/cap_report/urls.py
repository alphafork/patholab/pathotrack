from django.urls import path

from .views.cap_report_view import CAPReportCreate, CAPReportUpdate # call the view classes here


app_name = 'cap-reports'
urlpatterns = [
    path('create/', CAPReportCreate.as_view(), name='create'),
    path('update/', CAPReportUpdate.as_view(), name='update'),
]
