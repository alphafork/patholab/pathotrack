# Generated by Django 3.0.8 on 2020-09-13 23:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cap_report', '0003_delete_imageupload'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nonsynoptic',
            name='non_synoptic_diagnosis',
            field=models.TextField(blank=True, null=True, verbose_name='Diagnosis'),
        ),
    ]
