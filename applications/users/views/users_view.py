from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, TemplateView
from django.views.generic import DetailView, ListView, UpdateView
from django.shortcuts import redirect, get_object_or_404
from django.http import HttpResponseForbidden
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from applications.general_report.models.start_diagnosis_model import StartDiagnosis
from applications.patient_registration.models.patient_registration_model import PatientRegistration
from django.db.models import Q
from utils.mixins import SessionManagementMixin
from datetime import date, timedelta

class UserLoginView(SessionManagementMixin, LoginView):
    template_name = 'user_login.html'
    redirect_authenticated_user=True

class UserLogoutView(SessionManagementMixin, LoginRequiredMixin, LogoutView):
    template_name = 'user_logout.html'

class UserPasswordUpdate(SessionManagementMixin, LoginRequiredMixin, PasswordChangeView):
    template_name='user_password.html'
    success_url = reverse_lazy('users:profile')


class UserDashboardView(SessionManagementMixin, LoginRequiredMixin, ListView):
    model = StartDiagnosis
    template_name = 'user_dashboard.html'
    paginate_by = 10  # if pagination is desired
    '''
    need to replicate the code from consolidated_report list view with hints patient_registration_list
    '''

    def get_context_data(self, **kwargs): # appending report lists to patient
        last_week = date.today()-timedelta(days=7)
        context = super().get_context_data(**kwargs)
        context['wip_reports'] = self.model.objects.order_by('patient_enter_date').reverse().values('patient_id_id', 'patient_id__patient_name', 'pathology_id', 'patient_id__patient_phone', 'report_type__value', 'patient_enter_date', 'patient_sample', 'acc_no').filter( Q(report_status = 'wip') | Q(report_status = 'draft') )
        context["wip_length"] = len(context['wip_reports']);
        context['reports'] = self.model.objects.order_by('patient_enter_date').reverse().values('patient_id_id', 'patient_id__patient_name', 'pathology_id','patient_id__patient_phone', 'report_type__value', 'patient_enter_date', 'patient_sample', 'acc_no').filter(patient_exit_date__gte=last_week)
        context["recent_length"] = len(context['reports']);
        return context


    def post(self, request):
        submit_stat = self.request.POST.get("submit")
        acc_no = self.request.POST.get("acc_no")
        patient_id = self.request.POST.get("patient_id")

        self.request.session['acc_no'] = acc_no
        self.request.session['patient_id'] = patient_id

        if(submit_stat == "view"):
            return redirect('consolidated-reports:details')
        if(submit_stat == "edit"):
            return redirect('consolidated-reports:edit')
        if(submit_stat == "print"):
            return redirect('consolidated-reports:print')

        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]



class UserProfileDetails(SessionManagementMixin, LoginRequiredMixin, DetailView):
    model = User
    context_object_name = 'user'
    template_name = 'user_profile.html'

    def get_object(self): # to get object with pk with patient id
        user_id = self.request.user.id
        return get_object_or_404(self.model, pk=user_id)

    def post(self, request):
        if(request.POST.get("submit") == "dashboard"):
            return redirect('users:dashboard')
        if(request.POST.get("submit") == "password"):
            return redirect('users:password')
        if(request.POST.get("submit") == "edit"):
            return redirect('users:update')



class UserProfileUpdate(SessionManagementMixin, LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'user_profile_update.html'
    fields = ["username","first_name","last_name","email"]

    def get_object(self): # to get object with pk with patient id
        user_id = self.request.user.id
        return get_object_or_404(self.model, pk=user_id)

    def form_valid(self, form):
        form.save()
        return redirect('users:profile')
