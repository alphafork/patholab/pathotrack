"""patholab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from .views.users_view import UserLoginView, UserLogoutView, UserDashboardView, UserPasswordUpdate, UserProfileDetails, UserProfileUpdate


app_name = "users"

urlpatterns = [
    path('', UserLoginView.as_view(),name='login'),
    path('logout/', UserLogoutView.as_view(), name='logout'),
    path('dashboard/', UserDashboardView.as_view(), name='dashboard'),
    path('password/', UserPasswordUpdate.as_view(), name='password'),
    path('profile/', UserProfileDetails.as_view(), name='profile'),
    path('profile/edit/', UserProfileUpdate.as_view(), name='update'),
]
