from django.apps import AppConfig


class GeneralReportConfig(AppConfig):
    name = 'applications.general_report'
    verbose_name = 'General Report'
