from django.views.generic import CreateView, UpdateView
from applications.general_report.models.start_diagnosis_model import StartDiagnosis, ReportType
from applications.patient_registration.models.patient_registration_model import PatientRegistration
from django.shortcuts import redirect, get_object_or_404
from django.http import HttpResponse # for testing
from datetime import datetime
from django.contrib import messages
from uuid import uuid4
from utils.mixins import SessionManagementMixin


class StartDiagnosisCreate(SessionManagementMixin, CreateView):
    model = StartDiagnosis
    fields = "__all__"
    template_name = "start_diagnosis_form.html"
    session_vars =  ['patient_id','acc_no','patient_name','patient_age','patient_sex']


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pathology_id_part'] = datetime.now().strftime("%d%m%y")+"/"+str(self.model.objects.filter(patient_enter_date__gte=datetime.today().date()).count()+1)
        return context



    def form_valid(self, form):
        submit_stat = self.request.POST.get("submit")
        report_type = self.request.POST.get("report_type")
        pathology_temp_id = self.request.POST.get("pathology_id")
        report_type_str = ReportType.objects.get(id = report_type).value
        pathology_id_str = datetime.now().strftime("%d%m%y")+"/"+str(self.model.objects.filter(patient_enter_date__gte=datetime.today().date()).count()+1)

        if(submit_stat == "cancel"): # when "Cancel" button pressed
            return redirect('patients:details')

        acc_no = str(uuid4())
        form.instance.acc_no = acc_no
        form.instance.patient_id = PatientRegistration.objects.get(pk=self.request.session['patient_id'])

        # last_id = StartDiagnosis.objects.last()
        # pathology_id = datetime.now().strftime("%y") + str(StartDiagnosis.objects.last() + 1)
        #context['pathology_id_part'] = datetime.now().strftime("%y%m%d")+"/"+str(model.objects.filter(patient_enter_date__gte=datetime.today().date()).count()+1)
#        pathology_id = datetime.now().strftime("%y%m%d%H%M%S")
        pathology_id = report_type_str[:1]+"-"+pathology_id_str

        if pathology_temp_id != pathology_id:
            messages.error(self.request, 'Pathology ID mismatch, please try again!')
            return redirect('general-reports:start-diagnosis')

        form.instance.pathology_id = pathology_id
        report = form.save()

        if(submit_stat == "save"): # when pressed "Submit & Return"
            return redirect('patients:details')

        if(submit_stat == "continue"): # when pressed "Submit & Continue"
            self.request.session['acc_no'] = acc_no # passing acc_no to general report
            return redirect('general-reports:create')

        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]

class StartDiagnosisUpdate(UpdateView):
    model = StartDiagnosis
    fields = "__all__"
    template_name = "start_diagnosis_form.html"
    session_vars =  ['patient_id','acc_no','patient_name','patient_age','patient_sex']

    def get_object(self):
        return get_object_or_404(self.model, pk=self.request.session['acc_no'])

    def form_valid(self, form):
        StartDiagnosis.objects.filter(pk=self.request.session['acc_no']).update(report_status="draft")
        submit_stat = self.request.POST.get("submit")

        if(submit_stat == "cancel"):
            return redirect('consolidated-reports:details')

        report = form.save()
        if(submit_stat == "save"):
            return redirect('consolidated-reports:details')

        if(submit_stat == "continue"):
            return redirect('consolidated-reports:details') # Should be changed to redirect to update screen or create screen of general-reports [FIX_ME]

        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]
