from django.views.generic import CreateView, UpdateView
from applications.general_report.models.general_report_model import GeneralReport, Module_name, Sub_module
from applications.general_report.models.start_diagnosis_model import StartDiagnosis
from applications.patient_registration.models.patient_registration_model import PatientRegistration
from django.shortcuts import redirect, get_object_or_404
from django.http import HttpResponse # for testing
from utils.mixins import SessionManagementMixin
import importlib



# Class to create patient entry using CreateView
class GeneralReportCreate(SessionManagementMixin, CreateView):
    model = GeneralReport
    fields = "__all__"
    template_name = "general_report_form.html"
    session_vars =  ['patient_id','acc_no','patient_name','patient_age','patient_sex']
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pathology_id'] = StartDiagnosis.objects.get(pk=self.request.session['acc_no']).pathology_id
        return context

    def get(self, request, *args, **kwargs):
        StartDiagnosis.objects.filter(pk=self.request.session['acc_no']).update(report_status="wip")
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        submit_stat = self.request.POST.get("submit")
        acc_no = self.request.session['acc_no']
        patient_id = self.request.session['patient_id']

        if(submit_stat == "cancel"): # when "Cancel" button pressed
            StartDiagnosis.objects.filter(pk=acc_no).update(report_status="draft")
            return redirect('patients:details') # the condition where pressing cancel on filled entries has to be tested [TODO]

        form.instance.acc_no = StartDiagnosis.objects.get(pk=acc_no)
        form.instance.patient_id = PatientRegistration.objects.get(pk=patient_id)
        report = form.save()

        if(submit_stat == "save"): # when pressed "Submit & Return"
            StartDiagnosis.objects.filter(pk=acc_no).update(report_status="draft")
            return redirect('patients:details')

        if(submit_stat == "continue"): # when pressed "Submit & Continue"
            self.request.session['acc_no'] = acc_no
            self.request.session['sub_module'] = self.request.POST.get("sub_module")

            # For synoptic Reports
            if(self.request.POST.get("synoptic_stat") == "true"):

                module_id = self.request.POST.get("module_name")
                module_name = Module_name.objects.get(pk=module_id).value
                sub_module_id = self.request.POST.get("sub_module")
                sub_module = Sub_module.objects.get(pk=sub_module_id).value
                self.request.session['model_name'] = "applications.cap_report.models." + module_name.lower().replace(" ","_") + "_" + sub_module.lower().replace(" ","_") + "_model"
                self.request.session['model_class'] = module_name.replace(" ","") + sub_module.replace(" ","")
                return redirect('cap-reports:create')

            # For non-synoptic Reports
            self.request.session['model_name'] = "applications.cap_report.models.non_synoptic_model"
            self.request.session['model_class'] = "NonSynoptic"
            return redirect('cap-reports:create')

        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]


class GeneralReportUpdate(SessionManagementMixin, UpdateView):
    model = GeneralReport
    template_name = "general_report_form.html"
    fields = "__all__"
    session_vars =  ['patient_id','acc_no','patient_name','patient_age','patient_sex']

    def get_object(self):
        StartDiagnosis.objects.filter(pk=self.request.session['acc_no']).update(report_status="wip")
        return get_object_or_404(self.model, pk=self.request.session['acc_no'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pathology_id'] = StartDiagnosis.objects.get(pk=self.request.session['acc_no']).pathology_id
        return context

    def model_import(self, module_name, sub_module, synoptic_stat):
        if(synoptic_stat == "true" or synoptic_stat == True):
            model_name = "applications.cap_report.models." + module_name.lower().replace(" ","_") + "_" + sub_module.lower().replace(" ","_") + "_model"
            model_class = module_name.replace(" ","") + sub_module.replace(" ","")
        else:
            model_name = "applications.cap_report.models.non_synoptic_model"
            model_class = "NonSynoptic"
        return model_name, model_class

    def form_valid(self, form):
        submit_stat = self.request.POST.get("submit")
        patient_id = self.request.session['patient_id']
        acc_no = self.request.session['acc_no']

        if(submit_stat == "cancel"):
            StartDiagnosis.objects.filter(pk=acc_no).update(report_status="draft")
            return redirect('consolidated-reports:details')

        if(submit_stat == "save"):
            StartDiagnosis.objects.filter(pk=acc_no).update(report_status="draft")
            report = form.save()
            return redirect('consolidated-reports:details')

        if(submit_stat == "continue"):
            module_name_id = self.request.POST.get("module_name")
            module_name = Module_name.objects.get(pk=module_name_id).value
            sub_module_id = self.request.POST.get("sub_module")
            sub_module = Sub_module.objects.get(pk=sub_module_id).value
            synoptic_stat = self.request.POST.get("synoptic_stat")
            model_name_str, model_class_str = self.model_import(module_name, sub_module, synoptic_stat)

            model_name = importlib.import_module(model_name_str)
            model_class = getattr(model_name, model_class_str)          
            self.request.session['model_name'] = model_name_str
            self.request.session['model_class'] = model_class_str

            if(model_class.objects.filter(pk = acc_no).exists()):
                '''if there is an entry exists for the model_class
                entry from the form input this will redirect to
                update screen'''

                report = form.save()
                return redirect('cap-reports:update')


            if (GeneralReport.objects.get(pk = acc_no).sub_module_id == None
                or GeneralReport.objects.get(pk = acc_no).synoptic_stat == None
                or GeneralReport.objects.get(pk = acc_no).module_name_id == None):
                '''if there is no value for sub_module, synoptic_stat,
                model_name previously set, this will direct to cap_report
                createview without going through the code to delete
                previous cap_report'''

                report = form.save()
                return redirect('cap-reports:create')


            if (GeneralReport.objects.get(pk = acc_no).sub_module_id != sub_module_id
                or GeneralReport.objects.get(pk = acc_no).synoptic_stat != synoptic_stat
                or GeneralReport.objects.get(pk = acc_no).module_name_id != module_name_id):
                '''to delete old cap_report'''

                gen_sub_module = GeneralReport.objects.get(pk = acc_no).sub_module.value
                gen_module_name = GeneralReport.objects.get(pk = acc_no).module_name.value
                gen_synoptic_stat = GeneralReport.objects.get(pk = acc_no).synoptic_stat
                gen_model_name, gen_model_class = self.model_import(gen_module_name, gen_sub_module, gen_synoptic_stat)
                gen_model_name = importlib.import_module(gen_model_name)
                gen_model_class = getattr(gen_model_name, gen_model_class)

                if(gen_model_class.objects.filter(pk = acc_no).exists()):
                    gen_model_class.objects.get(pk = acc_no).delete()


            report = form.save()
            return redirect('cap-reports:create')

        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]
