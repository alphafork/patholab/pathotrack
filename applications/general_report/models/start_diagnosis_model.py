from django.db import models
from applications.patient_registration.models.patient_registration_model import PatientRegistration


# class PathologyIDManager(models.Manager):
#     def get_queryset(self):
#         return super().get_queryset().values('pathology_id')


class ReportType(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class StartDiagnosis(models.Model):
    class Meta:
        db_table = 'diagnosis_initiation'

    STATUS = [('wip','WIP'),('draft','Draft'),('submitted','Submitted'),('approved','Approved')]
    acc_no = models.CharField('Accession number', primary_key=True, editable=False, max_length=255)
    patient_id = models.ForeignKey(PatientRegistration, verbose_name="Patient ID", editable=False, on_delete=models.CASCADE)
    patient_sample = models.CharField('Sample', max_length=255, null=True, blank=True)
    patient_ref_clinic = models.CharField('Reference clinic', max_length=255, null=True, blank=True)
    patient_ref_doctor = models.CharField('Reference doctor', max_length=255, null=True, blank=True)
    patient_external_ref = models.CharField('External reference', max_length=255, null=True, blank=True)
    patient_enter_date = models.DateTimeField('Enter date', auto_now_add=True, null=True)
    patient_exit_date = models.DateTimeField('Exit date', auto_now=True, null=True)
    report_type = models.ForeignKey(ReportType, verbose_name='Report type', on_delete=models.CASCADE)
    pathology_id = models.CharField('Pathology ID', editable=False, max_length=255, null=True)
    report_status = models.CharField('Report status', choices=STATUS, default='draft', editable=False, max_length=10)
    # date_created =
    # date_updated =
    # user_created =
    # user_updated =
    # user_verified =

    # pathologyid_manager = PathologyIDManager()

    def get_fields(self):
        return [(field.verbose_name, field.value_from_object(self)) for field in self.__class__._meta.fields]
