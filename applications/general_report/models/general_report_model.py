from django.db import models
from .start_diagnosis_model import StartDiagnosis
from applications.patient_registration.models.patient_registration_model import PatientRegistration


class Module_name(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class Sub_module(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class ReportTypeManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().values('pathology_id')


class GeneralReport(models.Model):
    class Meta:
        db_table = 'general_reports'

    acc_no = models.OneToOneField(StartDiagnosis, on_delete=models.CASCADE, primary_key=True, editable=False)
    patient_id = models.ForeignKey(PatientRegistration, on_delete=models.CASCADE, editable=False)
    clinical_data = models.TextField(null=True,  blank=True)
    specimen = models.CharField(max_length=255, null=True, blank=True)
    macroscopy = models.TextField(null=True, blank=True)
    microscopy = models.TextField(null=True, blank=True)
    module_name = models.ForeignKey(Module_name, on_delete=models.CASCADE, null=True, blank=True)
    sub_module = models.ForeignKey(Sub_module, on_delete=models.CASCADE, null=True, blank=True)
    synoptic_stat = models.BooleanField('Synoptic report', null=True, blank=True)
    # date_created =
    # date_updated =
    # user_created =
    # user_updated =

    def get_fields(self):
        return [(field.verbose_name, field.value_from_object(self)) for field in self.__class__._meta.fields]
