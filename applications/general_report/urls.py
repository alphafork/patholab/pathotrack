from django.urls import path

from .views.general_report_view import GeneralReportCreate, GeneralReportUpdate # call the view classes here
from .views.start_diagnosis_view import StartDiagnosisCreate, StartDiagnosisUpdate

app_name = "general-reports"
urlpatterns = [
    path('start/', StartDiagnosisCreate.as_view(), name='start-diagnosis'),
    path('update-diagnosis/', StartDiagnosisUpdate.as_view(), name='update-diagnosis'),
    path('create/', GeneralReportCreate.as_view(), name='create'),
    path('update/', GeneralReportUpdate.as_view(), name='update'),
]
