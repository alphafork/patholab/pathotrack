# Generated by Django 3.0.8 on 2020-09-06 19:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('general_report', '0007_auto_20200905_0726'),
    ]

    operations = [
        migrations.AlterField(
            model_name='startdiagnosis',
            name='report_type',
            field=models.ForeignKey(default=99, on_delete=django.db.models.deletion.CASCADE, to='general_report.ReportType', verbose_name='Report type'),
            preserve_default=False,
        ),
    ]
