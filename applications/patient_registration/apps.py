from django.apps import AppConfig


class PatientRegistrationConfig(AppConfig):
    name = 'applications.patient_registration'
    verbose_name = 'Patient Registration'
