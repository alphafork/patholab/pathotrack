from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView, TemplateView
from applications.patient_registration.models.patient_registration_model import PatientRegistration
from applications.general_report.models.general_report_model import StartDiagnosis # For getting a list of reports under a patient_id
from django.shortcuts import redirect, get_object_or_404
from django.http import HttpResponseForbidden
from django_datatables_view.base_datatable_view import BaseDatatableView
from random import randrange
from django.urls import reverse_lazy
from uuid import uuid4
from django.utils.html import format_html
from django.middleware import csrf
from utils.mixins import SessionManagementMixin
from django.contrib import messages

# Class to create patient entry using CreateView
class PatientRegistrationCreate(SessionManagementMixin, CreateView):
    model = PatientRegistration
    template_name = "patient_registration_form.html"
    fields = "__all__"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        patient_id = str(uuid4())
        form.instance.patient_id = patient_id
        form.save()
        messages.success(self.request, 'Patient registered successfully!')
        return redirect('patients:list')


# Patient list view datatable
class PatientRegistrationListJson(BaseDatatableView):
    model = PatientRegistration # [FIXME] all the data is not required
    columns = ['slno','patient_name','patient_age','patient_sex','patient_phone','patient_reg_date','patient_city','actions']

    def render_column(self, row, column):
        if column == 'actions':
            csrf_token = csrf.get_token(self.request)
            return format_html('<form method="post"><input type="hidden" name="csrfmiddlewaretoken" value="{}"><input type="hidden" name="pat_id" value="{}"><button class="btn btn-sm btn-success" type="submit" name="submit" value="view"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-eye" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"/><circle cx="12" cy="12" r="2" /><path d="M2 12l1.5 2a11 11 0 0 0 17 0l1.5 -2" /><path d="M2 12l1.5 -2a11 11 0 0 1 17 0l1.5 2" /></svg>View</button>  <button class="btn btn-sm btn-warning" type="submit" name="submit" value="edit"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-edit" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"/><path d="M9 7 h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3" /><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3" /><line x1="16" y1="5" x2="19" y2="8" /></svg>Edit</button>  <button class="btn btn-sm btn-danger" type="submit" name="submit" value="delete"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-trash" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"/><line x1="4" y1="7" x2="20" y2="7" /><line x1="10" y1="11" x2="10" y2="17" /><line x1="14" y1="11" x2="14" y2="17" /><path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" /><path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /></svg>Delete</button></form>',csrf_token,row.id)
        elif column == 'patient_reg_date':
            return format_html('{}',row.patient_reg_date.strftime("%Y-%m-%d"))
        elif column == 'slno':
            return format_html('1')
        else:
            return super(PatientRegistrationListJson, self).render_column(row, column)


class PatientRegistrationList(SessionManagementMixin, TemplateView):
    template_name = 'patient_registration_list.html'

    def post(self, request):  # Taking post request contents from the template
        if(request.POST.get("submit") == "add"):
            return redirect('patients:register')

        patient_id = request.POST.get("pat_id")
        print(request.POST)
        self.request.session['patient_id'] = patient_id # Saving to session

        if(request.POST.get("submit") == "view"):
            return redirect('patients:details')
        elif(request.POST.get("submit") == "edit"):
            return redirect('patients:update')
        elif(request.POST.get("submit") == "delete"):
            return redirect('patients:delete')
        else:
            del self.request.session['patient_id']
            return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]


# Patient detail view
class PatientRegistrationDetails(SessionManagementMixin, DetailView):
    model = PatientRegistration
    context_object_name = 'patient'
    template_name = "patient_registration_details.html"
    session_vars = ['patient_id']

    def get_object(self): # to get object with pk with patient id
        return get_object_or_404(self.model, pk=self.request.session['patient_id'])

    def get_context_data(self, **kwargs): # appending report lists to patient
        context = super().get_context_data(**kwargs)
        context['reports'] = StartDiagnosis.objects.order_by('patient_enter_date').reverse().values('patient_id_id', 'pathology_id', 'report_status', 'patient_enter_date', 'patient_sample', 'acc_no').filter(patient_id_id = self.request.session['patient_id'] )

        # context['general_reports'] = GeneralReport.all().filter(patient_id = self.patient_id) # adding general reports with current patient_id to the context
        return context

    def post(self, request):  # Taking post request contents from the template
        # if 'general_report_id' in request.POST: # if there is a general_report_id in the post request, it is to report details screen
        #     self.request.session['general_report_id'] = request.POST.get('report_id')
        #     return redirect('report-details')

        # else:

        patient_id = request.POST.get("patient_id")
        acc_no = request.POST.get("acc_no")
        self.request.session['patient_id'] = patient_id

        if(request.POST.get("submit") == "report"):
            patient = PatientRegistration.objects.get(pk = patient_id)
            self.request.session['patient_name'] = patient.patient_name
            self.request.session['patient_age'] = patient.patient_age
            self.request.session['patient_sex'] = patient.patient_sex
            return redirect('general-reports:start-diagnosis')

        if(request.POST.get("submit") == "list"):
            return redirect('patients:list')
        if(request.POST.get("submit") == "delete"):
            return redirect('patients:delete')
        if(request.POST.get("submit") == "edit"):
            return redirect('patients:update')

        if(request.POST.get("submit") == "report-view"):
            self.request.session['patient_id'] = patient_id
            self.request.session['acc_no'] = acc_no
            return redirect('consolidated-reports:details')

        del self.request.session['patient_id']
        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]


# Class to update patient entry using update view
class PatientRegistrationUpdate(SessionManagementMixin, UpdateView):
    model = PatientRegistration
    template_name = "patient_registration_form.html"
    fields = "__all__"
    session_vars = ['patient_id']

    def get_object(self):
        return get_object_or_404(self.model, pk=self.request.session['patient_id'])

    def form_valid(self, form):
        report = form.save()
        patient_id = form.instance.id
        self.request.session['patient_id'] = patient_id
        return redirect('patients:details')


class PatientRegistrationDelete(SessionManagementMixin, DeleteView):
    model = PatientRegistration
    template_name = "patient_registration_delete.html"
    context_object_name = 'patient'
    success_url = reverse_lazy('patients:list')
    session_vars =  ['patient_id']

    def get_object(self):
        return get_object_or_404(self.model, pk=self.request.session['patient_id'])

    def post(self, request, *args, **kwargs):
        patient_id = request.POST.get("patient_id")
        self.request.session['patient_id'] = patient_id

        if(request.POST.get("submit") == "details"):
            return redirect('patients:details')
        if(request.POST.get("submit") == "delete"):
            return super().post(request, *args, **kwargs)

        del self.request.session['patient_id']
        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]
