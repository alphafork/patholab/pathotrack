from django.urls import path

from .views.patient_registration_view import PatientRegistrationCreate, PatientRegistrationDetails, PatientRegistrationList, PatientRegistrationListJson, PatientRegistrationUpdate, PatientRegistrationDelete # call the view classes here

app_name = "patients"
urlpatterns = [
    path('register/', PatientRegistrationCreate.as_view(), name='register'),
    path('update/', PatientRegistrationUpdate.as_view(), name='update'),
    path('details/', PatientRegistrationDetails.as_view(), name='details'),
    path('list/', PatientRegistrationList.as_view(), name='list'),
    path('datalist/', PatientRegistrationListJson.as_view(), name='datalist'),
    path('delete/', PatientRegistrationDelete.as_view(), name='delete'),
]
