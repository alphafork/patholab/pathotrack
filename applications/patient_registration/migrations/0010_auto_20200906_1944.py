# Generated by Django 3.0.8 on 2020-09-06 19:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('patient_registration', '0009_auto_20200904_0453'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_address1',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='address line 1'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_address2',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='address line 2'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_age',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='age'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_city',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='city'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_district',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='patient_registration.District'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_dob',
            field=models.DateField(blank=True, null=True, verbose_name='date of birth'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_email',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='email'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_id',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='UID'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='name'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_phone',
            field=models.BigIntegerField(blank=True, null=True, verbose_name='phone number'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_phone2',
            field=models.BigIntegerField(blank=True, null=True, verbose_name='phone number (2)'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_reg_date',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='registration date'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_sex',
            field=models.CharField(choices=[('M', 'Male'), ('F', 'Female'), ('O', 'Other')], default='M', max_length=1, null=True, verbose_name='sex'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_state',
            field=models.ForeignKey(blank=True, default=12, null=True, on_delete=django.db.models.deletion.CASCADE, to='patient_registration.State'),
        ),
        migrations.AlterField(
            model_name='patientregistration',
            name='patient_title',
            field=models.CharField(choices=[('M', 'Mr'), ('F', 'Ms'), ('B', 'Baby of'), ('D', 'Dr')], default='F', max_length=1, null=True, verbose_name='title'),
        ),
    ]
