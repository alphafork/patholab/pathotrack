from django.db import models
from django.utils import timezone
from django.urls import reverse



class State(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class District(models.Model):
    value = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class PatientRegistration(models.Model):

    class Meta:
        db_table = 'patient_details'

    TITLE = [('M','Mr'),('F','Ms'),('B','Baby of'),('D','Dr')]
    GENDER = [('M','Male'),('F','Female'),('O','Other')]
    patient_title = models.CharField('Title', max_length=1, choices=TITLE, default='F', null=True)
    patient_name = models.CharField('Name', max_length=255, null=True, blank=True)
    patient_id = models.CharField('UID', max_length=255, null=True, blank=True, editable=False)
    patient_sex = models.CharField('Sex', max_length=1, choices=GENDER, default='M', null=True)
    patient_dob = models.DateField('Date of birth', null=True, blank=True)
    patient_age = models.SmallIntegerField('Age', null=True, blank=True)
    patient_reg_date = models.DateTimeField('Registration date', auto_now=True, null=True, blank=True)
    patient_address1 = models.CharField('Address line 1', max_length=255, null=True, blank=True)
    patient_address2 = models.CharField('Address line 2', max_length=255, null=True, blank=True)
    patient_city = models.CharField('City', max_length=255, null=True, blank=True)
    patient_district = models.ForeignKey(District, verbose_name="District", on_delete=models.CASCADE, null=True, blank=True)
    patient_state = models.ForeignKey(State, verbose_name="State", on_delete=models.CASCADE, default=12, null=True, blank=True)
    patient_phone = models.BigIntegerField('Phone number', null=True, blank=True)
    patient_phone2 = models.BigIntegerField('Phone number (2)', null=True, blank=True)
    patient_email = models.EmailField('Email', null=True, blank=True)

    def get_fields(self):
        return [(field.verbose_name, field.value_from_object(self)) for field in self.__class__._meta.fields]
