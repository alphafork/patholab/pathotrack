from django.views.generic import DetailView, TemplateView
from applications.patient_registration.models.patient_registration_model import PatientRegistration
from applications.general_report.models.general_report_model import GeneralReport, Module_name, Sub_module
from applications.general_report.models.start_diagnosis_model import StartDiagnosis
from django.shortcuts import redirect, get_object_or_404
from django.http import HttpResponseForbidden
from django.utils.html import format_html
from django.middleware import csrf
from django_datatables_view.base_datatable_view import BaseDatatableView
from django_weasyprint import WeasyTemplateResponseMixin
from datetime import datetime
from utils.mixins import SessionManagementMixin
import importlib


class ConsolidatedReportDetails(SessionManagementMixin, DetailView):
    context_object_name = 'report'
    template_name = 'consolidated_report_details.html'
    session_vars =  ['patient_id','acc_no']

    def get_object(self):
        acc_no = self.request.session['acc_no']

        try:
            module_name = str(GeneralReport.objects.get(pk = acc_no).module_name)
            sub_module = str(GeneralReport.objects.get(pk = acc_no).sub_module)
            synoptic_stat = GeneralReport.objects.get(pk = acc_no).synoptic_stat
        except GeneralReport.DoesNotExist:
            return None

        try:
            if(synoptic_stat):
                model_name = "applications.cap_report.models." + module_name.lower().replace(" ","_") + "_" + sub_module.lower().replace(" ","_") + "_model"
                model_class = module_name.replace(" ","") + sub_module.replace(" ","")
            else:
                model_name = "applications.cap_report.models.non_synoptic_model"
                model_class = "NonSynoptic"

            model_name = importlib.import_module(model_name)
            model_class = getattr(model_name, model_class)
            model_object = model_class.objects.get(pk = acc_no)
            return model_object

        except:
            return None


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        acc_no = self.request.session['acc_no']

        try:
            initial_data = StartDiagnosis.objects.get(pk = acc_no)
        except StartDiagnosis.DoesNotExist:
            intial_data = None

        try:
            general_data = GeneralReport.objects.get(pk = acc_no)
        except GeneralReport.DoesNotExist:
            general_data = None

        context['initial_data'] = initial_data
        context['general_data'] = general_data
        return context

    def post(self, request):
        patient_id = self.request.session['patient_id']
        acc_no = self.request.session['acc_no']
        submit_stat = self.request.POST.get("submit")

        if(submit_stat == "report-list"):
            return redirect('consolidated-reports:list')
        if(submit_stat == "report-print"):
            return redirect('consolidated-reports:print-preview')
        if(submit_stat == "report-download"):
            return redirect('consolidated-reports:print')

        if(submit_stat == "patient-edit"):
            return redirect('patients:update')
        if(submit_stat == "patient-list"):
            return redirect('patients:list')

        patient = PatientRegistration.objects.get(pk = patient_id)
        self.request.session['patient_name'] = patient.patient_name
        self.request.session['patient_age'] = patient.patient_age
        self.request.session['patient_sex'] = patient.patient_sex
        
        if(submit_stat == "initial-data-edit"):
            return redirect('general-reports:update-diagnosis')

        if(submit_stat == "report-edit"):
            if(GeneralReport.objects.filter(pk = acc_no).exists()):
                return redirect('general-reports:update')
            else:
                return redirect('general-reports:create')

        return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]


class ConsolidatedReportListJson(BaseDatatableView):
    model = StartDiagnosis # [FIXME] all the data is not required
    columns = ['slno','patient_id.patient_name','pathology_id','patient_sample','histologic_type','patient_enter_date','patient_id.patient_phone','report_status','actions']

    # def get_context_data(self, *args, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     # histologic_type = self.histologic_type_response(acc_no)
    #     # context['histologic_type'] = histologic_type
    #     print(context['data'])
    #     return context

    def histologic_type_response(self, acc_no):
        try:
            module_name = str(GeneralReport.objects.get(pk = acc_no).module_name)
            sub_module = str(GeneralReport.objects.get(pk = acc_no).sub_module)

            model_name = "applications.cap_report.models." + module_name.lower().replace(" ","_") + "_" + sub_module.lower().replace(" ","_") + "_model"
            model_class = module_name.replace(" ","") + sub_module.replace(" ","")

            try:
                self.model_name = importlib.import_module(model_name)
                self.model_class = getattr(self.model_name, model_class)
            except:
                histologictype = 'None'

            if(self.model_class):
                try:
                    histologictype = self.model_class.objects.get(pk = acc_no).histologic_type
                    #histologictype = acc_no

                except:
                    histologictype = 'None'

            else:
                histologictype = 'None'


        except GeneralReport.DoesNotExist:
            histologictype =  'None'

        return histologictype



    def render_column(self, row, column):
        if column == 'actions':
            csrf_token = csrf.get_token(self.request)
            return format_html('<form method="post"><input type="hidden" name="csrfmiddlewaretoken" value="{}"><input type="hidden" name="acc_no" value="{}"><input type="hidden" name="patient_id" value="{}"><button class="btn btn-sm btn-success" type="submit" name="submit" value="view"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-eye" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"/><circle cx="12" cy="12" r="2" /><path d="M2 12l1.5 2a11 11 0 0 0 17 0l1.5 -2" /><path d="M2 12l1.5 -2a11 11 0 0 1 17 0l1.5 2" /></svg>View</button>  <button class="btn btn-sm btn-warning" type="submit" name="submit" value="edit"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-edit" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"/><path d="M9 7 h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3" /><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3" /><line x1="16" y1="5" x2="19" y2="8" /></svg>Edit</button></form>',csrf_token,row.acc_no,row.patient_id_id)
        elif column == 'slno':
            return format_html('1')
        elif column == 'histologic_type':
            histologic_type = self.histologic_type_response(row.acc_no)
            return format_html('{}',histologic_type)

        elif column == 'patient_enter_date':
            return format_html('{}',row.patient_enter_date.strftime("%Y-%m-%d"))
        elif column == 'report_status':
            if row.report_status == 'draft':
                badge_color = 'pink'
            elif row.report_status == 'wip':
                badge_color = 'orange'
            elif row.report_status == 'submitted':
                badge_color = 'blue'
            else:
                badge_color = 'red'
            return format_html('<span class="badge bg-{} ml-2 px-2">'+
                               '{}'+
                               '</span>',badge_color,row.report_status)
        else:
            return super(ConsolidatedReportListJson, self).render_column(row, column)


class ConsolidatedReportList(SessionManagementMixin, TemplateView):
    template_name = 'consolidated_report_list.html'

    def post(self, request):  # Taking post request contents from the template
        acc_no = request.POST.get("acc_no")
        patient_id = request.POST.get("patient_id")
        self.request.session['acc_no'] = acc_no # Saving to session
        self.request.session['patient_id'] = patient_id # Saving to session

        if(request.POST.get("submit") == "view"):
            return redirect('consolidated-reports:details')
        elif(request.POST.get("submit") == "edit"):
            return redirect('general-reports:update')
        else:
            del self.request.session['patient_id']
            return HttpResponseForbidden() # improve this later by adding invalid request template[FIX_ME]


class ConsolidatedReportPrint(WeasyTemplateResponseMixin, ConsolidatedReportDetails):
    template_name = 'consolidated_report_print.html'
    session_vars =  ['patient_id','acc_no']


class ConsolidatedReportPDF(ConsolidatedReportPrint):
    def get_pdf_filename(self):
        acc_no = self.request.session['acc_no']
        patient_name =  StartDiagnosis.objects.get(pk=acc_no).patient_id.patient_name
        curr_time = datetime.now().strftime('%Y%m%d%H%M')
        file_name = patient_name.title() + '-' + acc_no + '-' + curr_time + '.pdf'
        return file_name
