from django.urls import path

from .views.consolidated_report_view import ConsolidatedReportDetails, ConsolidatedReportList, ConsolidatedReportListJson, ConsolidatedReportPrint, ConsolidatedReportPDF # call the view classes here

app_name = 'consolidated-reports'
urlpatterns = [
    path('details/', ConsolidatedReportDetails.as_view(), name='details'),
    path('datalist/', ConsolidatedReportListJson.as_view(), name='datalist'),
    path('list/', ConsolidatedReportList.as_view(), name='list'),
    path('print-preview/', ConsolidatedReportPrint.as_view(), name='print-preview'),
    path('print/', ConsolidatedReportPDF.as_view(), name='print'),
]
