from django.db import models

class ConsolidatedReport(models.Model):
    class Meta:
        db_table = 'consolidated_reports'
