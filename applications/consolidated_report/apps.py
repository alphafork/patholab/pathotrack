from django.apps import AppConfig


class ConsolidatedReportConfig(AppConfig):
    name = 'applications.consolidated_report'
